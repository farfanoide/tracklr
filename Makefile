.PHONY: help update develop sdist release clean docs

help:
	@echo "help    - this help"
	@echo "update  - pip install -U -r requirements.txt"
	@echo "develop - hack tracklr"
	@echo "sdist   - package"
	@echo "release - package + upload release"
	@echo "clean   - remove build artifacts"
	@echo "docs    - generate HTML documentation"

update:
	@pip install -U -r requirements.txt

develop:
	python setup.py develop

sdist:
	python setup.py sdist

release:
	rm -rf dist build
	python setup.py sdist upload

clean:
	rm -rf dist build *.egg-info
	cd docs && make clean

docs:
	cd docs && rm -rf build && make html
