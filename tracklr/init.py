import os
import appdirs
import jinja2
import logging

from cliff.command import Command

from tracklr import Tracklr
from tracklr.pdf import Pdf


class Init(Command):

    log = logging.getLogger(__name__)

    tracklr = Tracklr()

    def take_action(self, parsed_args):
        """Puts tracklr.yml and pdf.html to:
        * user config directory
        * local directory
        """
        actions = {"config": self.init_config, "template": self.init_template}
        action = parsed_args.action
        self.log.info("initializing {}".format(action))
        actions[action](parsed_args)

    def init_config(self, parsed_args):
        """Creates local or global config.
        """
        self.create_file_type(
            parsed_args, self.tracklr.config_file, self.tracklr.__config__
        )

    def init_template(self, parsed_args):
        """Creates local or global template.
        """
        self.create_file_type(
            parsed_args, self.tracklr.pdf_template_file, Pdf.__template__
        )

    def create_file_type(self, parsed_args, file_name, file_content):
        """Handles creation of given file.
        """
        if parsed_args.user_config_dir:
            self.log.info("user config dir")
            self.create_file(
                parsed_args, self.tracklr.global_path, file_name, file_content
            )
        else:
            self.log.info("current dir")
            self.create_file(
                parsed_args, self.tracklr.local_path, file_name, file_content
            )

    def create_file(self, parsed_args, file_path, file_name, file_content):
        f = os.path.join(file_path, file_name)
        if os.path.isfile(f):
            self.log.info(
                "file {} already exists. " "nothing to do here...".format(f)
            )
        else:
            if not os.path.exists(file_path):
                self.log.info(
                    "directory {} does not exist. "
                    "creating...".format(file_path)
                )
                os.makedirs(file_path)
            self.log.info("file {} does not exist. " "creating...".format(f))
            fd = open(f, "w")
            fd.write(file_content)
            fd.close()

    def get_parser(self, prog_name):
        parser = super(Init, self).get_parser(prog_name)
        parser.add_argument(
            "action",
            type=str,
            choices=["config", "template"],
            help="init config/template" " in the current directory",
        )
        parser.add_argument(
            "--user-config-dir",
            action="store_true",
            help="create config/template in {} "
            "instead of the current directory".format(
                self.tracklr.global_path
            ),
        )
        return parser

    def get_description(self):
        return "initializes tracklr.yml and pdf.html"

    def generate_html(self):
        loader = jinja2.FileSystemLoader(searchpath=self.TEMPLATE_PATH)
        env = jinja2.Environment(loader=loader)
        template = env.get_template(self.TEMPLATE_REPORT)
        return template.render(tracklr=self.tracklr)

    def generate_pdf(self, in_html, out_pdf):
        report_file = open(out_pdf, "w+b")
        status = pisa.CreatePDF(in_html, dest=report_file)
        report_file.close()
