# -*- coding: utf-8 -*-
import os
import re
import appdirs
import logging
import requests
import yaml

from icalendar import Calendar
from requests.auth import HTTPBasicAuth
from tracklr.__version__ import VERSION


class Tracklr(object):
    """Tracklr loads events recorded in `iCalendar` feeds and
    uses them to create reports.
    """

    __version__ = VERSION

    __config__ = """
---
# Tracklr Instance Log Level (debug, info, warning, error, critical)
log_level: info

# List of calendars
#
# calendar attributes:
# * url               - mandatory - ical calendar feed
# * name              - optional - use `default` for your default calendar ie. useful for single calendar users
# * title/subtitle    - optional  - info used by `ls` and `pdf` commands
# * username/password - optional  - for BasicHTTPAuth protected feeds
#
calendars:
  # Tracklr demo calendar - simplest single calendar config v1
  - https://calendar.google.com/calendar/ical/bdtrpfi80dtav668iqd38oqi7g%40group.calendar.google.com/public/basic.ics

  # Tracklr demo calendar - simplest single calendar config v2
  #- url: https://calendar.google.com/calendar/ical/bdtrpfi80dtav668iqd38oqi7g%40group.calendar.google.com/public/basic.ics

  # Tracklr demo calendar - minimal default config
  #- name: minimal
  #  url: https://calendar.google.com/calendar/ical/bdtrpfi80dtav668iqd38oqi7g%40group.calendar.google.com/public/basic.ics

  # Tracklr demo calendar - full config
  #- name: full
  #  url: https://calendar.google.com/calendar/ical/bdtrpfi80dtav668iqd38oqi7g%40group.calendar.google.com/public/basic.ics
  #  title: Tracklr Demo
  #  subtitle: Report

  # 
  #- name: 
  #  url: 
  #  username: 
  #  password: 
        """

    def __init__(self):
        """Initializes Tracklr object with its configuration.
        """
        self.log_levels = {
            "DEBUG": logging.DEBUG,
            "INFO": logging.INFO,
            "WARNING": logging.WARNING,
            "ERROR": logging.ERROR,
            "CRITICAL": logging.CRITICAL,
        }

        self.calendars = dict()

        self.report = []
        self.report_html = []

        self.tags = dict()

        self.total_seconds = 0.0
        self.total_hours = 0.0

        self.pdf_template_file = "pdf.html"
        self.pdf_output_file = "report.pdf"

        self.local_path = os.getcwd()
        self.global_path = os.path.join(appdirs.user_config_dir(), "tracklr")

        self.template_path = [self.local_path, self.global_path]

        self.config_file = "tracklr.yml"

        self.loaded_config_file = None

        self.config = None
        self.configure()

    def configure(self):
        """Tries to load Tracklr configuration from current working directory
        then user config directory and if none found it defaults to internal
        configuration stored in ``Tracklr.__config__``.

        Once config loaded, processes ``calendars`` list from the config and
        handles various configuration options.
        """
        try:
            self.config = yaml.load(open(self.config_file, "r"))
            self.loaded_config_file = self.config_file
        except FileNotFoundError:
            try:
                config_file = os.path.join(self.global_path, self.config_file)
                self.config = yaml.load(open(config_file, "r"))
                self.loaded_config_file = config_file
            except FileNotFoundError:
                self.config = yaml.load(self.__config__)
                self.loaded_config_file = "default"

        self.get_logger()

        for cal in self.config["calendars"]:
            try:
                self.calendars[cal["name"]] = cal
            except KeyError:
                self.calendars["default"] = {
                    "name": "default",
                    "url": cal["url"],
                }
            except TypeError:
                self.calendars["default"] = {"name": "default", "url": cal}

        self.log.debug("config file: {}".format(self.config_file))
        self.log.debug("config:      {}".format(self.config))
        self.log.debug("calendars:   {}".format(self.calendars))

    def get_calendar_config(self, calendar):
        """Returns given calendar config or
        raises exception if none found.
        """
        if not calendar:
            calendar = "default"
        calendars = "\n".join([cal for cal in self.calendars])
        if calendar not in self.calendars:
            self.log.error(
                "calendar {} not found in configured calendars:\n{}".format(
                    calendar, calendars
                )
            )
            raise
        return self.calendars[calendar]

    def get_logger(self):
        """Sets up logger
        """
        logging.basicConfig()

        self.log = logging.getLogger(__name__)

        log_level = self.log_levels["INFO"]
        if "log_level" in self.config:
            log_level = self.log_levels[self.config["log_level"].upper()]

        self.log.setLevel(log_level)

        self.log.debug(
            "setting logging to {}".format(self.config["log_level"].upper())
        )

    def get_title(self, calendar, title):
        """Handles title of the provided calendar.

        Title is optional in the configuration so default title is "Tracklr".
        """
        self.title = "Tracklr"
        if title:
            self.title = title
        if "title" in self.calendars[calendar]:
            self.title = self.calendars[calendar]["title"]
        return self.title

    def get_subtitle(self, calendar, subtitle):
        """Handles title of the provided calendar.

        Title is optional in the configuration so default title is
        "Command-line Productivity Power Tool".
        """
        self.subtitle = "Command-line Productivity Power Tool"
        if subtitle:
            self.subtitle = subtitle
        if "subtitle" in self.calendars[calendar]:
            self.subtitle = self.calendars[calendar]["subtitle"]
        return self.subtitle

    def get_titles(self, calendar, title, subtitle):
        """Returns "title - subtitle" string.
        """
        cal = self.get_calendar_config(calendar)
        return "{} - {}".format(
            self.get_title(cal["name"], title),
            self.get_subtitle(cal["name"], subtitle),
        )

    def parse_tags(self, summary):
        """Parses given event summary and returns all #hashtags found.
        """
        tags = re.compile(r"#(\w+)")
        return tags.findall(summary)

    def get_pdf_output_file(self, file=None):
        """Return ``file`` is one is provided to input, or
        defaults to ``self.pdf_output_file = "report.pdf"``.
        """
        if file:
            return file
        return self.pdf_output_file

    def get_auth(self, username, password):
        """Returns ``HTTPBasicAuth`` for provided ``username`` and
        ``password``.
        """
        return HTTPBasicAuth(username, password)

    def get_feed(
        self,
        name,
        url,
        username=None,
        password=None,
        title=None,
        subtitle=None,
    ):
        """Loads calendar URL which can use BasicHTTPAuth.
        """
        if username and password:
            self.calendars[name]["auth"] = self.get_auth(username, password)
            resp = requests.get(url, auth=self.calendars[name]["auth"])
        else:
            resp = requests.get(url)
        if resp.status_code == 200:
            self.calendars[name]["ics"] = resp.text
            self.calendars[name]["calendar"] = Calendar.from_ical(
                self.calendars[name]["ics"]
            )

    def get_calendar(self, calendar):
        """Loads multiple calendars which can use BasicHTTPAuth.
        """
        cal = self.get_calendar_config(calendar)
        if "username" in cal and "password" in cal:
            self.get_feed(
                cal["name"], cal["url"], cal["username"], cal["password"]
            )
        else:
            self.get_feed(cal["name"], cal["url"])

    def get_event_length(self, event):
        """Calculates length of an event.
        """
        return event["DTEND"].dt - event["DTSTART"].dt

    def get_event_date(self, event, format="%Y-%m-%d"):
        """Returns dates(s) of given event.
        """
        s = event["DTSTART"].dt
        e = event["DTEND"].dt
        if s.year == e.year and s.month == e.month and s.day == e.day:
            return s.strftime(format)
        else:
            return "{} - {}".format(s.strftime(format), e.strftime(format))

    def filter_event(self, event, date_filter, client, project, tag):
        """Decides whether the event should be included or not.
        """
        date = self.get_event_date(event)
        if date_filter is not None and date_filter not in date:
            return True
        if (
            client is not None
            and client.lower() not in event["SUMMARY"].lower()
        ):
            return True
        if (
            project is not None
            and project.lower() not in event["SUMMARY"].lower()
        ):
            return True
        if tag is not None:
            filter_out = False
            tags = self.parse_tags(tag)
            for t in tags:
                if "#{}".format(t) not in event["SUMMARY"].lower():
                    filter_out = True
            if filter_out:
                return True
        return False

    def get_report(
        self,
        calendar=None,
        date_filter=None,
        client=None,
        project=None,
        tag=None,
    ):
        """Generates timesheet report in format::

            date, summary, description, hours
        """
        self.get_calendar(calendar)
        self.report = []
        self.report_html = []
        cal = self.get_calendar_config(calendar)

        for event in cal["calendar"].walk("vevent"):

            if self.filter_event(event, date_filter, client, project, tag):
                continue

            try:
                summary = event["SUMMARY"]
            except KeyError:
                summary = ""
            try:
                description = event["DESCRIPTION"]
            except KeyError:
                description = ""

            date = self.get_event_date(event)
            lent = self.get_event_length(event)
            entry = (date, summary, description, lent.total_seconds() / 3600.0)
            self.report.append(entry)

            entry_html = (
                date,
                str(summary).replace("\n", "<br />"),
                str(description).replace("\n", "<br />"),
                lent.total_seconds() / 3600.0,
            )
            self.report_html.append(entry_html)

            self.total_seconds = self.total_seconds + lent.total_seconds()
            self.total_hours = self.total_seconds / 3600.0
        self.report = sorted(self.report)
        self.report_html = sorted(self.report_html)
        return self.report

    def get_tags(
        self,
        calendar=None,
        date_filter=None,
        client=None,
        project=None,
        tag=None,
    ):
        """Generates tags report in format::

            tag, hours
        """
        self.get_calendar(calendar)
        self.report = []
        cal = self.get_calendar_config(calendar)

        for event in cal["calendar"].walk("vevent"):

            if self.filter_event(event, date_filter, client, project, tag):
                continue

            try:
                summary = event["SUMMARY"]
                tags = self.parse_tags(summary)
                if tags:
                    lent = self.get_event_length(event)
                    for t in tags:
                        if t in self.tags:
                            self.tags[t] += lent.total_seconds() / 3600.0
                        else:
                            self.tags[t] = lent.total_seconds() / 3600.0
                    self.total_seconds = (
                        self.total_seconds + lent.total_seconds()
                    )
                    self.total_hours = self.total_seconds / 3600.0
            except KeyError:
                self.log.debug("No summary found")

        if self.tags:
            for t in self.tags:
                entry = (t, self.tags[t])
                self.report.append(entry)
        else:
            self.log.info("No tags found")

        self.report = sorted(self.report)
        return self.report

    def get_parser(self, parser):
        """Returns parser with Tracklr's arguments:

        * ``-k`` ``--calendar``
        * ``-d`` ``--date-filter``
        * ``-t`` ``--title``
        * ``-s`` ``--subtitle``
        * ``client``
        * ``project``
        * ``tag``
        """
        parser.add_argument("-k", "--calendar")
        parser.add_argument("-d", "--date-filter")
        parser.add_argument("-t", "--title")
        parser.add_argument("-s", "--subtitle")
        parser.add_argument("client", nargs="?", default="")
        parser.add_argument("project", nargs="?", default="")
        parser.add_argument("tag", nargs="?", default="")
        return parser
