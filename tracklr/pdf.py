import jinja2
import logging

from xhtml2pdf import pisa
from cliff.command import Command
from tracklr import Tracklr
from jinja2 import Template
from jinja2.exceptions import TemplateNotFound


class Pdf(Command):

    log = logging.getLogger(__name__)

    tracklr = Tracklr()

    __template__ = """
<html>
  <header>
    <title>{{ tracklr.title }} {{ tracklr.subtitle }}</title>
    <style>
    table {
        font: 12px Verdana, Arial, Helvetica, sans-serif;
        table-layout: fixed;
    }

    th {
        padding: 0.5em 0.5em 0;
        border-bottom: 3px solid #555;
    }
    th.date {
        text-align: left;
        width: 100px;
    }
    th.summary {
        text-align: left;
    }
    th.hours {
        text-align: right;
        width: 100px;
    }

    td {
        border-bottom: 1px solid #CCC;
        padding: 0.25em 0.5em 0;
        vertical-align: top;
    }
    td.date {
        text-align: left;
        width: 100px;
    }
    td.summary {
        text-align: left;
    }
    td.hours {
        text-align: right;
        width: 100px;
    }
    tr.total {
        border-top: 2px solid #555;
        font-weight: bold;
    }
    </style>
  </header>
  <body>
    <h1>{{ tracklr.title }} - {{ tracklr.subtitle }}</h1>
    <table>
      <tr>
        <th class="date">Date</th>
        <th class="summary">Summary</th>
        <th class="hours">Hours</th>
      </tr>
      {% for item in tracklr.report_html %}
      <tr>
        <td class="date">{{ item.0 }}</td>
        <td class="summary">{{ item.2 }}</td>
        <td class="hours">{{ item.3 }}</td>
      </tr>
      {% endfor %}
      <tr class="total">
        <td colspan="2">Total</td>
        <td class="hours">{{ tracklr.total_hours }}</td>
      </tr>
    </table>
  </body>
</html>
        """

    def take_action(self, parsed_args):
        """Generates report as a PDF file.
        """
        cal = self.tracklr.get_calendar_config(parsed_args.calendar)
        self.tracklr.get_calendar(cal["name"])
        self.tracklr.get_report(
            parsed_args.calendar,
            parsed_args.date_filter,
            parsed_args.client,
            parsed_args.project,
            parsed_args.tag,
        )

        self.tracklr.get_title(cal["name"], parsed_args.title)
        self.tracklr.get_subtitle(cal["name"], parsed_args.subtitle)

        in_html = self.generate_html()

        out_pdf = self.tracklr.get_pdf_output_file(parsed_args.file)

        titles = self.tracklr.get_titles(
            parsed_args.calendar, parsed_args.title, parsed_args.subtitle
        )
        self.log.info(
            "Generating...\n{}\nOutput file: {}".format(titles, out_pdf)
        )

        self.generate_pdf(in_html, out_pdf)

    def get_description(self):
        return "creates PDF report"

    def get_parser(self, prog_name):
        parser = super(Pdf, self).get_parser(prog_name)
        parser.add_argument("-f", "--file")
        return self.tracklr.get_parser(parser)

    def generate_html(self):
        try:
            loader = jinja2.FileSystemLoader(
                searchpath=self.tracklr.template_path
            )
            env = jinja2.Environment(loader=loader)
            template = env.get_template(self.tracklr.pdf_template_file)
            return template.render(tracklr=self.tracklr)
        except TemplateNotFound:
            template = Template(self.__template__)
            return template.render(tracklr=self.tracklr)

    def generate_pdf(self, in_html, out_pdf):
        report_file = open(out_pdf, "w+b")
        status = pisa.CreatePDF(in_html, dest=report_file)
        report_file.close()
