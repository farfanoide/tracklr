import logging

from cliff.lister import Lister
from tracklr import Tracklr


class Ls(Lister):

    log = logging.getLogger(__name__)

    tracklr = Tracklr()

    def take_action(self, parsed_args):
        """Generates report and logs total number of hours.
        """
        ts = self.tracklr.get_report(
            parsed_args.calendar,
            parsed_args.date_filter,
            parsed_args.client,
            parsed_args.project,
            parsed_args.tag,
        )

        titles = self.tracklr.get_titles(
            parsed_args.calendar, parsed_args.title, parsed_args.subtitle
        )
        self.log.info(titles)
        self.log.info("Total hours: {}".format(self.tracklr.total_hours))

        return (("Date", "Summary", "Description", "Hours"), ts)

    def get_description(self):
        return "creates report"

    def get_parser(self, prog_name):
        parser = super(Ls, self).get_parser(prog_name)
        return self.tracklr.get_parser(parser)
