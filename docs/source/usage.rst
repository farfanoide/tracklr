Usage
=====

``tracklr -h`` or ``tracklr --help`` displays usage page.

Adding ``--debug`` option helps to debug issues during development.

``tracklr ls --help`` or ``tracklr pdf --help`` displays help pages for available commands.


``ls``
------

``tracklr ls`` lists all events in calendar and provides total number of hours tracked in the
calendar.

Example::

    tracklr ls
    Total hours: 4.0
    +------------+----------+----------------------------------------------------------------+-------+
    | Date       | Summary  | Description                                                    | Hours |
    +------------+----------+----------------------------------------------------------------+-------+
    | 2019-02-09 | @Tracklr | Worked on initial version of https://gitlab.com/markuz/tracklr |   4.0 |
    +------------+----------+----------------------------------------------------------------+-------+


More examples::

    # show only 2019 events
    tracklr ls -d 2019

    # show only 2019-02 events
    tracklr ls -d 2019-02

    # show only 2019 @tracklr events
    tracklr ls -d 2019 @tracklr

    # show only 2019 `other` calendar @project events
    tracklr ls -d 2019 other @project

    # show all 2019 events from `calendar` for @client's +project that matches #tag
    tracklr ls -d 2019 calendar @client +project #tag


``tag``
-------

``tracklr tag`` creates reports of hours spend on ``#tagged`` events.

Example::

    tracklr tag @tra tra "#tags"
    Tracklr - Command-line Productivity Power Tool
    Total hours: 1.0
    +------+-------+
    | Tag  | Hours |
    +------+-------+
    | tags |   1.0 |
    +------+-------+


``pdf``
-------

``tracklr pdf`` creates PDF report which displays date, description and hours.

Example::

    # generate 2019 @tracklr PDF report 
    tracklr pdf -d 2019 @tracklr


See example file here: :download:`report.pdf <_static/report.pdf>`.


``init``
--------

``tracklr init {config,template}`` allows users to create ``tracklr.yml`` and ``pdf.html`` either
in the current working directory ie. where ``tracklr`` looks for these first, or
into so called user config directory eg. on Linux at ``~/.config/tracklr/``.

``tracklr`` looks for ``tracklr.yml`` and ``pdf.html`` firstly in the current working directory and
then in user config directory, or falls back to defaults.

By default ``tracklr`` uses its own configuration stored in ``Tracklr.__config__``.

For PDF reports ``tracklr`` uses by default its own HTML template in ``tracklr.pdf.Pdf.__template``.

Examples::

    # init tracklr.yml in current working dir
    tracklr init config

    # init pdf.html in user config dir
    tracklr init template --user-config-dir


``show``
--------

``tracklr show`` displays information about itself.
