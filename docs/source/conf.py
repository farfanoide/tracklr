# -*- coding: utf-8 -*-
from tracklr import Tracklr


project = 'Tracklr'
copyright = '2019, Marek Kuziel'
author = 'Marek Kuziel'
version = Tracklr.__version__
release = Tracklr.__version__
extensions = ["sphinx.ext.autodoc"]
templates_path = ['_templates']
source_suffix = '.rst'
master_doc = 'index'
language = None
exclude_patterns = []
pygments_style = "bw"
html_static_path = ['_static']
html_title = "Tracklr Documentation"
html_short_title = "Tracklr"
html_static_path = ['_static']
import guzzle_sphinx_theme
html_translator_class = 'guzzle_sphinx_theme.HTMLTranslator'
html_theme_path = guzzle_sphinx_theme.html_theme_path()
html_theme = 'guzzle_sphinx_theme'
html_sidebars = {
    '**': ['logo-text.html', 'globaltoc.html', 'searchbox.html']
}
extensions.append("guzzle_sphinx_theme")
html_theme_options = {
    "project_nav_name": "Tracklr",
    "projectlink": "https://tracklr.com",
}
htmlhelp_basename = 'Tracklrdoc'
html_show_sourcelink = False
html_show_sphinx = True
man_pages = [
    (master_doc, 'tracklr', 'Tracklr Documentation',
     [author], 1)
]
