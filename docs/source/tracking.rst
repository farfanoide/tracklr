Tracking
========

The core idea behind ``tracklr`` is to use `iCalendar` feeds for time tracking progress.

Usaul usage would be to track progress of work for clients, or personal projects, activities, etc.

Keywords to use in event summaries:

* ``@context`` - use ``@`` to identify context of events eg. ``@clients``, ``@garden``, ``@outdoors``.

* ``+project`` - use ``+`` to identify projects.

* ``#tag`` - use ``#`` for tagging ie. categorization of work.

* ``!user`` - use ``!`` to identify calendar users if `iCalendar` feed is used by multiple users.


Options
-------

1) One calendar to rule them all

   Pros:

   * simple
   * all ``@clients`` ``+projects`` ``#in-one-place``

   Cons:

   * not good multi users usage

-----

2) One calendar per ``@client``

   Pros:

   * good for billing

   Cons:

   * okay but not ideal for multi users usage

-----

3) One calendar per ``@client`` per ``!user``

   Pros:

   * clear who did what for whom

   Cons:

   * too complex
