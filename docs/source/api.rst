API Reference
=============

Sample code to illustrate ``Tracklr`` code functionality using a demo `iCalendar` feed.

.. code-block:: python

    >>> from tracklr import Tracklr
    >>> Tracklr.__version__
    '0.5'
    >>> t = Tracklr()
    >>> report = t.get_report()
    >>> for event in report:
    ...     print("{} | {} | {}".format(event[0], event[2], event[3]))
    ...     
    ... 
    2019-02-09 | Worked on initial version of https://gitlab.com/markuz/tracklr | 4.0
    2019-02-15 | * fixed timesheet sorting
    * fixed possible missing description/summary in ical feeds
    * refactored logging
    * started on v0.2 ie. multi calendar support | 2.0
    2019-02-15 | Testing tracklr v0.1 with Ivan and Tomas | 0.5
    2019-02-19 | v0.2 - support of mutliple calendars | 1.0
    2019-02-20 | worked on multi-calendar support | 0.5
    2019-02-21 | v0.3 - finished off implementation to support multiple calendars | 1.0
    2019-02-22 | v0.3 tidy ups | 1.0
    2019-02-22 | v0.4 added ``tracklr init {config,template}`` command for easy custom
    configuration of tracklr | 2.0
    >>> t.total_hours
    12.0
    >>> 

Tracklr
-------

.. automodule:: tracklr
      :members:
