Hacking
=======

To develop ``tracklr`` via ``pyenv`` + ``virtualenv``, run::

    pyenv virtualenv 3.7.2 tracklr              # create new virtualenv
    cd ~/.pyenv/versions/tracklr                # cd into new env
    source bin/activate                         # activate new env
    cd                                          # move home or wherever
    git clone https://gitlab.com/markuz/tracklr # clone tracklr repo
    cd tracklr                                  # get into the repo dir
    make update                                 # install dependencies
    make develop                                # build development egg
    make docs                                   # generate documentation
