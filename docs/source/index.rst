Tracklr
=======

    Command-line Productivity Power Tool

Tracklr loads events recorded in `iCalendar` feeds and uses them to create reports.

.. toctree::
   :maxdepth: 2

   README
   configuration
   tracking
   usage
   hacking
   api
   CHANGELOG


..
    Indices and tables
    ------------------
    * :ref:`genindex`
    * :ref:`modindex`
    * :ref:`search`
