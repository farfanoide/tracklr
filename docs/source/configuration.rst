Configuration
=============

Linux
-----

``tracklr.yml`` config file must be located either at the same directory from where ``tracklr`` 
command is run from, or at ``~/.config/tracklr/tracklr.yml`` ie. user configuration directory.

``tracklr`` searches for the config file in that order of preference ie. local first then user
config dir.

Out of the box (when there is no ``tracklr.yml`` file available)
``tracklr`` uses its own configuration stored in ``Tracklr.__config__``.

For PDF reports ``tracklr`` uses by default its own HTML template in ``tracklr.pdf.Pdf.__template``.

``tracklr`` provides ``init`` command to create ``tracklr.yml`` and ``pdf.html`` files either in
user config directory eg. ``~/.config/tracklr/`` or current working directory.

See ``tracklr init --help`` for more details.

Example ``tracklr.yml`` file:

.. code:: yaml

    ---
    # Tracklr Instance Log Level (debug, info, warning, error, critical)
    log_level: info
    
    # List of calendars
    #
    # calendar attributes:
    # * url               - mandatory - ical calendar feed
    # * name              - optional - use `default` for your default calendar ie. useful for single calendar users
    # * title/subtitle    - optional  - info used by `ls` and `pdf` commands
    # * username/password - optional  - for BasicHTTPAuth protected feeds
    #
    calendars:
      # Tracklr demo calendar - simplest single calendar config v1
      - https://calendar.google.com/calendar/ical/bdtrpfi80dtav668iqd38oqi7g%40group.calendar.google.com/public/basic.ics
    
      # Tracklr demo calendar - simplest single calendar config v2
      #- url: https://calendar.google.com/calendar/ical/bdtrpfi80dtav668iqd38oqi7g%40group.calendar.google.com/public/basic.ics
    
      # Tracklr demo calendar - minimal default config
      #- name: minimal
      #  url: https://calendar.google.com/calendar/ical/bdtrpfi80dtav668iqd38oqi7g%40group.calendar.google.com/public/basic.ics
    
      # Tracklr demo calendar - full config
      #- name: full
      #  url: https://calendar.google.com/calendar/ical/bdtrpfi80dtav668iqd38oqi7g%40group.calendar.google.com/public/basic.ics
      #  title: Tracklr Demo
      #  subtitle: Report
    
      # 
      #- name: 
      #  url: 
      #  username: 
      #  password: 


PDF Template
------------

To be able to generate PDF version of a report, ``pdf.html`` template must be located either at
current working directory or at user config directory.

For PDF reports ``tracklr`` uses by default its own HTML template in ``tracklr.pdf.Pdf.__template``.

User ``tracklr init template`` to generate ``pdf.html`` into current working directory, or
``tracklr init template --user-config-dir`` for creation under user config directory.

``tracklr`` looks first at the current working dir, then to user config dir and if nothing found
then it falls back to ``tracklr.pdf.Pdf.__template``.
