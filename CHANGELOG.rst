CHANGELOG
=========

2019-03-02 | v0.6
-----------------

* added ``tracklr tag`` command
* new default pdf html template
* added sorting of html report


2019-02-24 | v0.5
-----------------

* added ``tracklr show`` command to display information about ``tracklr`` configuration
* more flexible calendar configuration options for simpler setup


2019-02-23 | v0.4
-----------------

* added ``tracklr init {config,template}`` command so that users can create ``tracklr.yml`` and
  ``pdf.html`` either in current working directory or in user config directory. 
* fixed title/subtitle in ls and pdf commands
* renamed timesheet to report
* refactored config and pdf template to be loaded first from local dir, then user config dir or
  locally from Tracklr demo ie. defaults
* one version setting via ``Tracklr.__version__`` to rule them all


2019-02-22 | v0.3
-----------------

* finished off imlpementation to support multiple calendars
* updated documentation


2019-02-20 | v0.2
-----------------

* ``tracklr.yml`` config changed; users can use multiple calendars. See docs for details.
* Fixed missing ``DESCRIPTION`` in events. Thanks Ivan Müller for the report.


2019-02-09 | v0.1
-----------------

* Initial version
