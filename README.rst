Tracklr
=======

.. contents::
      :local:


Introduction
------------

``Tracklr`` is a command-line toolset for processing `iCalendar` feeds.


Installation
------------

Install ``tracklr`` via ``pip``::

    pip install https://gitlab.com/markuz/tracklr/-/archive/master/tracklr-master.tar.bz2


Configuration
-------------

Out of the box ``tracklr`` uses its own configuration stored in ``Tracklr.__config__``.

For PDF reports ``tracklr`` uses by default its own HTML template in ``tracklr.pdf.Pdf.__template__``.

``tracklr`` provides ``init`` command to create ``tracklr.yml`` and ``pdf.html`` files either in
user config directory eg. ``~/.config/tracklr/`` or current working directory (default).

See ``tracklr init --help`` for more details.


Usage
-----

::

    # setup local config
    tracklr init config

    # setup global pdf.html uses for all tracklr instances
    tracklr init template --user-config-dir

    # show configuration
    tracklr show

    # show only 2019-02 events
    tracklr ls -d 2019-02

    # show only 2019 @tracklr events
    tracklr ls -d 2019 @tracklr

    # generate 2019 @tracklr PDF report 
    tracklr pdf -d 2019 @tracklr

    # show all tagged hours
    tracklr tag @tra tra #tags


Documentation
-------------

See the ``docs`` folder in the code repository https://gitlab.com/markuz/tracklr/


Hacking
-------

Pull requests are welcome. For more information, see
`hacking docs <https://gitlab.com/markuz/tracklr/blob/master/docs/source/hacking.rst>`_


License
-------

`BSD 3-clause Clear License <https://gitlab.com/markuz/tracklr/blob/master/LICENSE>`_
